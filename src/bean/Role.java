package bean;

public enum Role {
  USER("lvl 1"),
  CUSTOMER("lvl 1"),
  ADMIN("lvl 2"),
  PROVIDER("lvl 2"),
  SUPER_ADMIN("lvl 3"),
  EMPTY_ROLE("lvl 0");
  private String level;

  Role(String level) {

    this.level = level;
  }
}

