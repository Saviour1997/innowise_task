package console;

import bean.Role;
import bean.User;
import bean.UserBuilder;
import bean.UserBuilderImpl;
import io.UserFileWriter;
import service.DataBaseService;
import java.util.List;
import java.util.Scanner;
import static bean.Role.USER;
import static console.Validator.*;

public class Menu {

  private UserBuilder userBuilder;
  private DataBaseService dataBaseService;


  private final String createNameMessage = "Input NAME and press 'Enter'";
  private final String createRoleMessage = """
      Input ROLE NUMBER :
      1. USER
      2. CUSTOMER
      3. PROVIDER
      4. ADMIN
      5. USER and ADMIN
      6. USER and PROVIDER\s
      7. CUSTOMER and PROVIDER\s
      8. CUSTOMER and ADMIN\s
      9. SUPER_ADMIN
      10. Exit""";

  private final String createMobileMessage =
      """
          1. Input Mobile1 Number
          2. Input Mobile2 Number
          3. Input Mobile3 Number
          4. Exit""";
  private final String firstEditMessage = "Input 1 to Input ID \n" +
      "Input 2 to leave menu";
  private final Scanner scanner;

  public Menu() {
    this.scanner = new Scanner(System.in);
    this.dataBaseService = new DataBaseService();
  }

  public void mainMenu() {
    int choice;
    while (true) {
      String startMessage = """
          1. Create user
          2. Edit user
          3. View all users
          4. Save changes
          5. Remove User By ID
          6. Exit""";
      choice = getAnswerFromMenu(startMessage, 6);
      switch (choice) {
        case 1:
          createUserMenu();
          break;
        case 2:
          editMenu();
          break;
        case 3:
          viewUsers();
          break;
        case 4:
          saveChanges();
          break;
        case 5:
          removeUser();
          break;
        case 6:
          return;
      }
    }
  }

  private void saveChanges() {
    List<User> users = dataBaseService.getUsersDataBase();
    UserFileWriter.writeToFile(users);
  }

  private int getAnswerFromMenu(String message, int menuSize) {
    String answer;
    int result;
    while (true) {
      System.out.println(message);
      if (scanner.hasNextLine()) {
        answer = scanner.nextLine();
        if (isNumber(answer)) {
          result = Integer.parseInt(answer);
          if (result <= menuSize && result > 0) {
            return result;
          } else {
            errorMenu("Please input digits from 1 to "
                + menuSize + "\npress any key to continue");

          }
        } else {
          errorMenu("Please input only digits "
              + "\npress any key to continue");
        }
      }
    }
  }

  private void errorMenu(String errorMessage) {
    System.out.println(errorMessage);
    scanner.nextLine();
  }

  public void viewUsers() {
    List<User> userList = dataBaseService.getUsersDataBase();
    for (User user : userList) {
      System.out.println(user);
    }

  }

  public void createUserMenu() {
    int choice;
    userBuilder = new UserBuilderImpl();
    while (true) {
      String createUserMessage = """
          1. Input User name
          2. Input User surname
          3. Input User e-mail
          4. Input User role1
          5. Input User phone
          6. Add user to DataBase
          7. Return to main menu""";
      choice = getAnswerFromMenu(createUserMessage, 7);
      switch (choice) {
        case 1:
          createNameMenu();
          break;
        case 2:
          createSurnameMenu();
          break;
        case 3:
          createEmailMenu();
          break;
        case 4:
          createRoleMenu();
          break;
        case 5:
          createMobilMenu();
          break;
        case 6:
          addUserToDataBase();
          break;
        case 7:
          return;
      }
    }
  }

  private void inputIdMenu() {
    String id = "";
    while (true) {
      System.out.println("Input ID of the user you want to be edited, and press 'ENTER'");
      if (scanner.hasNextLine()) {
        id = scanner.nextLine();
      }
      if (id.length() > 0 && isNumber(id)
          && dataBaseService.hasDataBaseUserById(Integer.parseInt(id))) {
        System.out.println(dataBaseService.getUserFromDataBase(Integer.parseInt(id)));
        editUserMenu(Integer.parseInt(id));
        return;
      } else {
        errorMenu("You do not Input correct ID Number, " +
            "\npress 'Enter' to try again");

      }
    }
  }

  private void inputIdMenuForRemove() {
    String id = "";
    while (true) {
      System.out.println("Input ID of the user you want to be removed, and press 'ENTER'");
      if (scanner.hasNextLine()) {
        id = scanner.nextLine();
      }
      if (id.length() > 0 && isNumber(id)
          && dataBaseService.hasDataBaseUserById(Integer.parseInt(id))) {
        System.out.println(dataBaseService.getUserFromDataBase(Integer.parseInt(id)));
        removeUserById(Integer.parseInt(id));
        return;
      } else {
        errorMenu("You don't Input correct ID Number, " +
            "\npress 'Enter' to try again");

      }
    }
  }

  private void editMenu() {
    int choice;
    while (true) {
      choice = getAnswerFromMenu(firstEditMessage, 2);
      switch (choice) {
        case 1:
          inputIdMenu();
          break;
        case 2:
          return;
      }
    }
  }

  private void removeUser() {
    int choice;
    while (true) {
      choice = getAnswerFromMenu(firstEditMessage, 2);
      switch (choice) {
        case 1:
          inputIdMenuForRemove();
          break;
        case 2:
          return;
      }
    }

  }

  private void removeUserById(int id) {
    int choice;
    while (true) {
      String validationRemoveMessage = """
          You are Removing User  Id you choose.\s
          Input - '1' - to remove
          Input - '2' - to leave this menu""";
      choice = getAnswerFromMenu(validationRemoveMessage, 2);

      switch (choice) {
        case 1:
          dataBaseService.removeUserFromDataBase(id);
          System.out.println("User Id = " + id + " successfully removed");
          break;
        case 2:
          return;
      }
    }


  }

  private void editUserMenu(int id) {
    int choice;
    //       userBuilder = new UserBuilderImpl();
    while (true) {
      String editUserMessage = """
          1. Edit chosen User name
          2. Edit chosen User surname
          3. Edit chosen User e-mail
          4. Edit chosen User role1
          5. Edit chosen User phone
          6. Return to main menu""";
      choice = getAnswerFromMenu(editUserMessage, 6);
      switch (choice) {
        case 1:
          editNameMenu(id);
          break;
        case 2:
          editSurnameMenu(id);
          break;
        case 3:
          editEmailMenu(id);
          break;
        case 4:
          editRoleMenu(id);
          break;
        case 5:
          editMobileMenu(id);
          break;
        case 6:
          return;
      }
    }
  }

  private void editNameMenu(int id) {
    String answer = "";
    while (true) {
      System.out.println(createNameMessage);
      if (scanner.hasNextLine()) {
        answer = scanner.nextLine();
      }
      if (answer.length() > 0 && isLetter(answer)) {
        dataBaseService.getUserFromDataBase(id).setName(answer);
        return;
      } else {
        errorMenu("You do not Input the Name, press 'Enter' to try again");
      }
    }
  }

  private void editSurnameMenu(int id) {
    String answer = "";
    while (true) {
      System.out.println(createNameMessage);
      if (scanner.hasNextLine()) {
        answer = scanner.nextLine();
      }
      if (answer.length() > 0 && isLetter(answer)) {
        dataBaseService.getUserFromDataBase(id).setSurname(answer);
        return;
      } else {
        errorMenu("You do not Input the SurName, press 'Enter' to try again");
      }
    }

  }

  private void editEmailMenu(int id) {
    String answer = "";
    while (true) {
      System.out.println(createNameMessage);
      if (scanner.hasNextLine()) {
        answer = scanner.nextLine();
      }
      if (answer.length() > 0 && isEmail(answer)) {
        dataBaseService.getUserFromDataBase(id).setEmail(answer);
        return;
      } else {
        errorMenu("You do not Input the Email, press 'Enter' to try again");
      }
    }
  }

  private void editRoleMenu(int id) {
    int choice;
    while (true) {
      choice = getAnswerFromMenu(createRoleMessage, 10);
      switch (choice) {
        case 1 -> {
          dataBaseService.getUserFromDataBase(id).setRole1(USER);
          dataBaseService.getUserFromDataBase(id).setRole2(Role.EMPTY_ROLE);
        }
        case 2 -> {
          dataBaseService.getUserFromDataBase(id).setRole1(Role.CUSTOMER);
          dataBaseService.getUserFromDataBase(id).setRole2(Role.EMPTY_ROLE);
        }
        case 3 -> {
          dataBaseService.getUserFromDataBase(id).setRole1(Role.PROVIDER);
          dataBaseService.getUserFromDataBase(id).setRole2(Role.EMPTY_ROLE);
        }
        case 4 -> {
          dataBaseService.getUserFromDataBase(id).setRole1(Role.ADMIN);
          dataBaseService.getUserFromDataBase(id).setRole2(Role.EMPTY_ROLE);
        }
        case 5 -> {
          dataBaseService.getUserFromDataBase(id).setRole2(Role.ADMIN);
          dataBaseService.getUserFromDataBase(id).setRole1(Role.USER);
        }
        case 6 -> {
          dataBaseService.getUserFromDataBase(id).setRole2(Role.PROVIDER);
          dataBaseService.getUserFromDataBase(id).setRole1(Role.USER);
        }
        case 7 -> {
          dataBaseService.getUserFromDataBase(id).setRole2(Role.PROVIDER);
          dataBaseService.getUserFromDataBase(id).setRole1(Role.CUSTOMER);
        }
        case 8 -> {
          dataBaseService.getUserFromDataBase(id).setRole2(Role.ADMIN);
          dataBaseService.getUserFromDataBase(id).setRole1(Role.CUSTOMER);
        }
        case 9 -> {
          dataBaseService.getUserFromDataBase(id).setRole1(Role.SUPER_ADMIN);
          dataBaseService.getUserFromDataBase(id).setRole2(Role.EMPTY_ROLE);
        }
        case 10 -> {
          System.out.println("You are exit to prev menu");
          return;
        }
      }
      System.out.println("The role # " + choice + " is chosen");
      return;
    }
  }

  private void editMobileMenu(int id) {
    int choice;
    while (true) {
      choice = getAnswerFromMenu(createMobileMessage, 4);
      switch (choice) {
        case 1:
          dataBaseService.getUserFromDataBase(id).setMobil1(getMobileData());
          break;
        case 2:
          dataBaseService.getUserFromDataBase(id).setMobil2(getMobileData());
          break;
        case 3:
          dataBaseService.getUserFromDataBase(id).setMobil3(getMobileData());
          break;
        case 4:
          return;
      }
    }
  }


  public void createNameMenu() {
    String answer = "";
    while (true) {
      System.out.println(createNameMessage);
      if (scanner.hasNextLine()) {
        answer = scanner.nextLine();
      }
      if (answer.length() > 0 && isLetter(answer)) {
        userBuilder.setName(answer);
        return;
      } else {
        errorMenu("You do not Input the Name, press 'Enter' to continue try again");
      }
    }
  }

  public void createSurnameMenu() {
    String answer = "";
    while (true) {
      String createSurNameMessage = "Input SURNAME and press 'Enter'";
      System.out.println(createSurNameMessage);
      if (scanner.hasNextLine()) {
        answer = scanner.nextLine();
      }
      if (answer.length() > 0 && isLetter(answer)) {
        userBuilder.setSurname(answer);
        return;
      } else {
        errorMenu("You do not Input the surname, press 'Enter' to continue try again");
      }
    }
  }

  public void createEmailMenu() {
    String answer = "";
    while (true) {
      String createEmailMessage = "Input EMAIL and press 'Enter'";
      System.out.println(createEmailMessage);
      if (scanner.hasNextLine()) {
        answer = scanner.nextLine();
      }
      if (answer.length() > 0 && isEmail(answer)) {
        userBuilder.setEmail(answer);
        return;
      } else {
        errorMenu("You do not Input the Email, press 'Enter' to continue try again");
      }
    }
  }


  public void createRoleMenu() {
    int choice;
    while (true) {
      choice = getAnswerFromMenu(createRoleMessage, 10);
      switch (choice) {
        case 1 -> {
          userBuilder.setRole1(Role.USER);
          userBuilder.setRole2(Role.EMPTY_ROLE);
        }
        case 2 -> {
          userBuilder.setRole1(Role.CUSTOMER);
          userBuilder.setRole2(Role.EMPTY_ROLE);
        }
        case 3 -> {
          userBuilder.setRole1(Role.PROVIDER);
          userBuilder.setRole2(Role.EMPTY_ROLE);
        }
        case 4 -> {
          userBuilder.setRole1(Role.ADMIN);
          userBuilder.setRole2(Role.EMPTY_ROLE);
        }
        case 5 -> {
          userBuilder.setRole2(Role.ADMIN);
          userBuilder.setRole1(Role.USER);
        }
        case 6 -> {
          userBuilder.setRole2(Role.PROVIDER);
          userBuilder.setRole1(Role.USER);
        }
        case 7 -> {
          userBuilder.setRole2(Role.PROVIDER);
          userBuilder.setRole1(Role.CUSTOMER);
        }
        case 8 -> {
          userBuilder.setRole2(Role.ADMIN);
          userBuilder.setRole1(Role.CUSTOMER);
        }
        case 9 -> {
          userBuilder.setRole1(Role.SUPER_ADMIN);
          userBuilder.setRole2(Role.EMPTY_ROLE);
        }
        case 10 -> {
          System.out.println("You are exit to previous menu");
          return;
        }
      }
      System.out.println("The role # " + choice + " is chosen");
      return;
    }
  }

  public void createMobilMenu() {
    int choice;
    while (true) {
      choice = getAnswerFromMenu(createMobileMessage, 4);
      switch (choice) {
        case 1:
          userBuilder.setMobil1(getMobileData());
          break;
        case 2:
          userBuilder.setMobil2(getMobileData());
          break;
        case 3:
          userBuilder.setMobil3(getMobileData());
          break;
        case 4:
          return;
      }
    }
  }

  private String getMobileData() {
    String answer = "";
    while (true) {
      System.out.println("Input Mobil Number like +375xxxxxxxxx");
      if (scanner.hasNextLine()) {
        answer = scanner.nextLine();
      }
      if (answer.length() > 0 && isPhone(answer)) {
        return answer;
      } else {
        errorMenu("You do not Input the Mobile Number, " +
            "\npress 'Enter' to try again");
      }
    }
  }

  private void addUserToDataBase() {
    User user = userBuilder.build();
    dataBaseService.addToDataBase(user);
    System.out.println("User saved successfully");
  }
}